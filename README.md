# firefoxvnc-oci
[Mozilla Firefox](https://www.mozilla.org/firefox/) in an OCI container. Accessible via VNC or a web browser.

## Supported Architectures
`amd64`

## Version Tags
| Tag | Description |
| :----: | --- |
| latest | Latest release. |

## Parameters
| Parameter | Function |
| :----: | --- |
| `-p 5900` | vnc server port |
| `-p 8080` | web server port |
| `-e SCREEN_WIDTH=1600` | screen width |
| `-e SCREEN_HEIGTH=900` | screen heigth |

## Setup
Note that Firefox will likely not run as root.
### docker cli
```
sudo docker run -d \
  --rm \
  --init \
  --user $(id -u):$(id -g) \
  -p 5900:5900 \
  -p 8080:8080 \
  -e SCREEN_WIDTH=1600 \
  -e SCREEN_HEIGTH=900 \
  registry.gitlab.com/sandervdamme/firefoxvnc-oci
```
### podman cli
```
podman run -d \
  --rm \
  --init \
  --userns keep-id \
  -p 5900:5900 \
  -p 8080:8080 \
  -e SCREEN_WIDTH=1600 \
  -e SCREEN_HEIGTH=900 \
  registry.gitlab.com/sandervdamme/firefoxvnc-oci
```

## Usage 
The VNC server is available at `vnc://<your-ip>:5900`.  
The web UI is available at `http://<your-ip>:8080/vnc.html`.  
Note that there is no authentication.

## Built with
- [Mozilla Firefox](https://www.mozilla.org/firefox/)
- [Sway](https://swaywm.org/)
- [wayvnc](https://github.com/any1/wayvnc)
- [noVNC](https://github.com/novnc/noVNC)
- [websockify](https://github.com/novnc/websockify)
- [Fedora](https://getfedora.org/)
- [Docker](https://www.docker.com/)

## License
BSD-3
